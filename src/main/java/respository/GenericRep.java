package respository;

import java.util.List;

import domain.BaseEntity;

public interface GenericRep<T extends BaseEntity<PK>, PK extends Number>
{
	public void save(T entity);

	public void update(T entity);

	public void remove(T entity);

	public void removeById(PK id);

	public T find(PK id);

	public long count();
	
	public boolean existsByUsername(String username);
	
	public boolean exists(PK id);

	public List<T> findAll();
}