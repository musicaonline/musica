package respository;

import java.util.List;

import domain.Administrador;


public interface AdmiRep extends GenericRep<Administrador, Long> {
	
	Administrador findByEmail(String email);
	List<Administrador> filterByEmail(String email);
	Long findIdByUsername(String username);
	boolean existsAdministrator(String username, String password);
	
}