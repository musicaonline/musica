package domain;

public class Album implements BaseEntity<Long>{

	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "Album";
	
	public String name;
	public String autor;
	public String description;
	public int time;	

	@Override
	public String toString() {
		String str="";
		str+="Name: "+getName()+"\n";
		str+="Autor: "+getAutor()+"\n";
		str+="Description: "+getDescription()+"\n";
		return str;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAutor() {
		return autor;
	}


	public void setAutor(String autor) {
		this.autor = autor;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getTime() {
		return time;
	}


	public void setTime(int time) {
		this.time = time;
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}
